import AppBar from "@material-ui/core/AppBar/AppBar";
import Tabs from "@material-ui/core/Tabs/Tabs";
import Tab from "@material-ui/core/Tab/Tab";
import {Link} from "react-router-dom";
import React from "react";


class MenuBar extends React.Component {

    render(){
        return (
        <div style={{marginBottom: "50px"}}>
            <AppBar position="fixed" style={{
                backgroundColor: " #ece8d9",
                color: "#494949",
            }}>
                <Tabs>
                    <Tab label="Home" component={Link} to={'/'}/>
                    <Tab label="About" component={Link} to={'/about'}/>
                    <Tab label="Portfolio" component={Link} to={'/portofolio'}/>
                    <Tab label="Resume" component={Link} to={'/resume'}/>
                    <Tab label="Contact" component={Link} to={'/contact'}/>
                </Tabs>
            </AppBar>
        </div>
        );
    }
}

export default MenuBar;