
import React from "react";
import Grid from "@material-ui/core/Grid/Grid";
import Paper from "@material-ui/core/Paper/Paper";
import Button from "@material-ui/core/Button/Button";
import { SocialIcon } from 'react-social-icons';
import TextField from '@material-ui/core/TextField';


class Footer extends React.Component {

    render(){
        return (
            <div >
                <Grid container spacing={24}>
                    <Grid item xs={1} sm={1} md={1} style={{backgroundColor: "#faf6e9"}}>
                    </Grid>
                    <Grid item xs={11} sm={11} md={3} style={{backgroundColor: "#faf6e9"}}>
                        <h4>About me</h4>
                        <br/> We have tested a number of registry fix and clean utilities and present our top 3 list on
                        our site for your convenience.

                    </Grid>
                    <Grid item xs={1} sm={1} md={1} style={{backgroundColor: "#faf6e9"}}>
                    </Grid>
                    <Grid item xs={11} sm={11} md={3} style={{backgroundColor: "#faf6e9"}}>
                        <h4> Newsletter</h4> <br/>
                        Stay updated with our latest trends. <br/>
                        <TextField
                            id="standard-name"
                            label="Email"
                            placeholder={"Enter email address"}
                            margin="normal"
                        />
                        <SocialIcon network="email" color="#565656" style={{margin:"10px"}}/>

                    </Grid>
                    <Grid item xs={1} sm={1} md={1} style={{backgroundColor: "#faf6e9"}}>
                    </Grid>
                    <Grid item xs={11} sm={11} md={2} style={{backgroundColor: "#faf6e9"}}>
                        <h4> Follow me </h4> <br/>

                        <SocialIcon url="https://www.linkedin.com/in/laura-pacurar-769506120/"  color="#565656" style={{margin:"10px"}}/>
                        <SocialIcon url="https://twitter.com/lauraa_maria22"  color="#565656" style={{margin:"10px"}}/>
                        <SocialIcon url="https://www.facebook.com/Lallly.Lully" color="#565656" style={{margin:"10px"}}/>

                    </Grid>
                    <Grid item xs={12} sm={12} md={1} style={{backgroundColor: "#faf6e9"}}>
                    </Grid>
                    <Grid item xs={12} sm={12} md={12} style={{backgroundColor: "#ece8d9", textAlign: "center"}}>
                        @2018 by Laura Pacurar
                    </Grid>

                </Grid>
            </div>
        );
    }
}
const styles = {
    paperContainer: {
        backgroundColor:`${props => props.theme.main.cream}`,
        position: "fixed",
        backgroundRepeat: "repeat-y",
        height: "auto",
        backgroundSize: "cover",
        backgroundPosition: "center",
        width: "auto",
        minWidth:"100%",
        minHeight:"100%",
        zIndex: "-100"
    },
    textOver: {
        backgroundColor: "black",
        color: "white",
        opacity: "0.70",
        //width: "100%",
        //height: "auto",
        textAlign: "center",
        // position: "relative",
        //  paddingTop: "5px",
        // paddingBottom: "10px",
        padding:"20px",
        fontSize: "300%",
        // marginTop: "50px"
    }
};


export default Footer;
