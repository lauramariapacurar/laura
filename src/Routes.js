import React from 'react';
import {BrowserRouter, Switch, Route} from 'react-router-dom';

import {ThemeProvider} from 'styled-components';
import theme from 'theme';
import GeneralLayout from "layouts/GeneralLayout";

import HomePage from 'pages/Home';
import About from 'pages/About';
import Resume from 'pages/Resume';
import Portofolio from 'pages/Portofolio';
import Contact from 'pages/Contact';

const Routes = () => {
    return (
        <ThemeProvider theme={theme}>
            <BrowserRouter>
                <Switch>
                    <Route exact path="/" component={GeneralLayout(HomePage)}/>
                    <Route path="/about" component={GeneralLayout(About)}/>
                    <Route path="/resume" component={GeneralLayout(Resume)}/>
                    <Route path="/portofolio" component={GeneralLayout(Portofolio)}/>
                    <Route path="/contact" component={GeneralLayout(Contact)}/>
                </Switch>
            </BrowserRouter>
        </ThemeProvider>
    )
};

export default Routes;