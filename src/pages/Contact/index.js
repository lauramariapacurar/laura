import React from 'react';
import styled from 'styled-components';
import Paper from "@material-ui/core/Paper/Paper";
import Image from 'material-ui-image';
import Button from '@material-ui/core/Button';
import Grid from "@material-ui/core/Grid/Grid";
import { SocialIcon } from 'react-social-icons';

import Map from '../../components/Map';
import MenuBar from "components/MenuBar";
import Footer from "components/Footer";

class Contact extends React.Component {

    render() {
        return (
            <div>
                <MenuBar />
                <Grid container spacing={24}>
                    <Grid item xs={12}>
                        <Paper style={styles.paperContainer}>
                        </Paper>
                    </Grid>

                    <Grid item xs={12} style={{marginTop: "50px", marginBottom:"5px"}}>
                        <div style={styles.textOver}>
                            LET'S HAVE A CHAT
                        </div>
                    </Grid>
                    <Grid item xs={12} sm={12} md={3}>
                    </Grid>
                    <Grid item xs={12} sm={12} md={6} style={{backgroundColor:"#565656", opacity:"0.85"}}>
                        <div >
                            <Map
                                id="myMap"
                                options={{
                                    center: { lat: 45.653630, lng: 25.608434 },
                                    zoom: 10
                                }}
                                onMapLoad={map => {
                                    const marker = new window.google.maps.Marker({
                                        position: { lat: 45.653630, lng: 25.608434 },
                                        map: map,
                                        title: 'Hello Brasov!'
                                    });
                                }}
                            />
                        </div>
                    </Grid>

                </Grid>
                <Grid container style={{marginTop:"30px", marginBottom:"10%"}}>
                    <Grid item xs={12} sm={12} md={3}  >
                    </Grid>
                    <Grid item xs={12} sm={12} md={6}>
                        <div style={{ fontSize:"110%", backgroundColor:"#ece8d9",opacity:"0.65"}} >

                            <br/> <br/>
                            <div style={{marginLeft:"15px"}}>
                                Reach out and send me a friendly hello.
                            </div>
                            <SocialIcon url="https://www.linkedin.com/in/laura-pacurar-769506120/"  color="#565656" style={{margin:"10px"}}/>
                            <SocialIcon url="https://twitter.com/lauraa_maria22"  color="#565656" style={{margin:"10px"}}/>
                            <SocialIcon url="https://www.facebook.com/Lallly.Lully" color="#565656" style={{margin:"10px"}}/>
                            <SocialIcon network="email" color="#565656" style={{margin:"10px"}}/> lauramariapacurar@yahoo.com
                            <br/>

                        </div>
                    </Grid>

                </Grid>
                <Footer />
            </div>
        );
    }
}

/*
const RedTitle = styled.h4`
    color: ${props => props.theme.color.red};
`;
*/
const styles = {
    paperContainer: {
        //backgroundImage: `url(${'Laptop.jpg'})`,
        backgroundColor: '#fffdf6',
        position: "fixed",
        backgroundRepeat: "repeat-y",
        height: "auto",
        backgroundSize: "cover",
        backgroundPosition: "center",
        width: "auto",
        minWidth:"100%",
        minHeight:"100%",
        zIndex: "-100"
    },
    textOver: {
        backgroundColor: "#494949",
        color: "white",
        opacity: "0.70",
        //width: "100%",
        //height: "auto",
        textAlign: "center",
        // position: "relative",
        //  paddingTop: "5px",
        // paddingBottom: "10px",
        padding:"20px",
        fontSize: "300%",
        // marginTop: "50px"
    }
};


export default Contact;