import React from 'react';
import styled from 'styled-components';
import Paper from "@material-ui/core/Paper/Paper";
import Image from 'material-ui-image';
import Button from '@material-ui/core/Button';
import Grid from "@material-ui/core/Grid/Grid";
import MenuBar from "components/MenuBar";
import Footer from "components/Footer";
import FormControl from '@material-ui/core/FormControl'

class Resume extends React.Component {

    render() {
        return (
            <div>
                <MenuBar />
                <Grid container spacing={24}>
                    <Grid item xs={12}>
                       <Paper style={styles.paperContainer}>
                       </Paper>
                    </Grid>

                    <Grid item xs={12} style={{marginTop: "50px", marginBottom:"5px"}}>
                        <div style={styles.textOver}>
                            RESUME
                        </div>
                    </Grid>

                </Grid>


                {/*Skills ------------------------------------------*/}

                <Grid container spacing={24} style={{paddingLeft:"20%", paddingRight:"20%", paddingTop:"5%"}}>
                    <Grid item xs={12} sm={12} md={2} style={{backgroundColor:"#faf6e9", paddingLeft:"10px"}}>
                         <div >
                           <h2><em>SKILLS</em> </h2>
                         </div>
                   </Grid>
                    <Grid item xs={12} sm={6} md={3} style={{backgroundColor:"#faf6e9"}}>
                        <div >
                            <h4>LANGUAGES </h4> <br/>
                            Php <br/>
                            C++ <br/>
                            CSS <br/>
                            JavaScript <br/>
                            HTML <br/>
                            SQL
                        </div>
                    </Grid>
                    <Grid item xs={12} sm={6} md={3} style={{backgroundColor:"#faf6e9"}}>
                        <div >
                            <h4>FRAMEWORKS </h4> <br/>
                            Laravel <br/>
                            React <br/>
                            Node

                        </div>
                    </Grid>
                    <Grid item xs={12} sm={6} md={2} style={{backgroundColor:"#faf6e9"}}>
                        <div >
                            <h4>TOOLS </h4> <br/>
                            Git & Github  <br/>
                            Chrome DevTools <br/>
                            npm
                        </div>
                    </Grid>
                    <Grid item xs={12} sm={6} md={2} style={{backgroundColor:"#faf6e9"}}>
                        <div >
                            <h4>DESIGN </h4> <br/>
                            InDesign <br/>
                            Adobe Photoshop
                        </div>
                    </Grid>
                    <Grid item xs={12}>
                        <div>

                        </div>
                    </Grid>
                </Grid>

                {/*Education---------------------------------------------------------------*/}

                < Grid container spacing={24} style={{paddingLeft:"20%", paddingRight:"20%"}}>
                    <Grid item xs={12} sm={12} md={2} style={{backgroundColor:"#faf6e9", paddingLeft:"10px",paddingTop:"35px"}}>
                        <div >
                            <h2><em>EDUCATION</em> </h2>
                        </div>
                    </Grid>
                    <Grid item xs={12} sm={6} md={3} style={{backgroundColor:"#faf6e9",paddingTop:"35px"}}>
                        <div >
                            Bachelor Degree
                        </div>
                    </Grid>
                    <Grid item xs={12} sm={6} md={2} style={{backgroundColor:"#faf6e9",paddingTop:"35px"}}>
                        <div >
                            oct 2014 - jul 2017

                        </div>
                    </Grid>
                    <Grid item xs={12} sm={6} md={3} style={{backgroundColor:"#faf6e9",paddingTop:"35px"}}>
                        <div >
                            Faculty of Mathematics and <br/> Computer Science <br/>
                            Bucharest, Romania
                        </div>
                    </Grid>
                    <Grid item xs={12} sm={6} md={2} style={{backgroundColor:"#faf6e9", paddingRight:"70px",paddingTop:"35px"}}>
                        <div >
                            Informatics
                        </div>
                    </Grid>
                        <Grid item xs={12} sm={12} md={2} style={{backgroundColor:"#faf6e9", paddingLeft:"70px",paddingTop:"35px"}}>

                        </Grid>
                        <Grid item xs={12} sm={6} md={3} style={{backgroundColor:"#faf6e9",paddingTop:"35px"}}>
                            <div >
                                Master Degree
                            </div>
                        </Grid>
                        <Grid item xs={12} sm={6} md={2} style={{backgroundColor:"#faf6e9",paddingTop:"35px"}}>
                            <div >
                                oct 2018 - present

                            </div>
                        </Grid>
                        <Grid item xs={12} sm={6} md={3} style={{backgroundColor:"#faf6e9",paddingTop:"35px"}}>
                            <div >
                                Faculty of Economics and <br/> Business Administration <br/>
                                Brasov, Romania
                            </div>
                        </Grid>
                        <Grid item xs={12} sm={6} md={2} style={{backgroundColor:"#faf6e9", paddingRight:"70px",paddingTop:"35px"}}>
                            <div >
                                Integrated Business <br/> Information Systems
                            </div>
                        </Grid>
                    <Grid item xs={12}>
                        <div>

                        </div>
                    </Grid>

                </Grid>

                {/*Experience ------------------------------------------------------------------*/}

                <Grid container spacing={24} style={{paddingLeft:"20%", paddingRight:"20%",paddingBottom:"4%"}}>
                    <Grid item xs={12} sm={12} md={2} style={{backgroundColor:"#faf6e9", padding:"5px",paddingTop:"35px"}}>
                        <div style={{paddingLeft:"10px"}}>
                            <h2><em>WORK EXPERIENCE</em> </h2>
                        </div>
                    </Grid>

                    <Grid item xs={12} sm={12} md={2} style={{backgroundColor:"#faf6e9", padding:"5px",paddingTop:"35px"}}>
                    </Grid>
                    <Grid item xs={12} sm={12} md={4} style={{backgroundColor:"#faf6e9", padding:"5px",paddingTop:"35px"}}>
                        <div >
                            <h4><b>Freelancer</b> </h4>
                        </div>
                    </Grid>

                    <Grid item xs={12} sm={12} md={4} style={{backgroundColor:"#faf6e9", padding:"5px",paddingTop:"35px"}}>
                        <div >
                            apr 2018 - present
                        </div>
                    </Grid>
                    <Grid item xs={12} sm={12} md={4} style={{backgroundColor:"#faf6e9", padding:"5px"}}>
                    </Grid>

                    <Grid item xs={12} sm={12} md={4} style={{backgroundColor:"#faf6e9", padding:"5px"}}>
                        <div >
                            <h4><b>Vodafone Romania - Engineer</b> </h4>
                        </div>
                    </Grid>
                    <Grid item xs={12} sm={12} md={4} style={{backgroundColor:"#faf6e9", padding:"5px"}}>
                        <div >
                            jan 2018 - march 2018
                        </div>
                    </Grid>
                    <Grid item xs={12} sm={12} md={4} style={{backgroundColor:"#faf6e9", padding:"5px"}}>
                    </Grid>
                    <Grid item xs={12} sm={12} md={4} style={{backgroundColor:"#faf6e9", padding:"5px"}}>
                        <div >
                            <h4><b>WorkPlace3</b> </h4>
                        </div>
                    </Grid>
                    <Grid item xs={12} sm={12} md={4} style={{backgroundColor:"#faf6e9", padding:"5px"}}>
                        <div >
                            march 2012 - apr 2015
                        </div>
                    </Grid>


                </Grid>
                <Grid container spacing={24}>
                    <Grid item xs={1} sm={2} md={5}>
                    </Grid>
                    <Grid item xs={9} sm={8} md={2} >

                            {/*<Button  style={{textAlign:"center", backgroundColor:"black", opacity:"0.65",color:"white",display:"flex", position:"relative",margin:"auto", marginTop:"5px", marginBottom:"10%"}} href={'Pacurar-Laura.pdf'} >*/}
                                {/*Download Resume*/}
                            {/*</Button>*/}
                          <div style={{textAlign:"center", backgroundColor:"black", opacity:"0.65",color:"white",display:"flex", position:"relative",margin:"auto", marginTop:"5px", marginBottom:"10%",padding:"10px", paddingLeft:"25%"}}>
                              <a  href={'Pacurar-Laura.pdf'} target="_blank" > Download Resume </a>
                              {/*<a  href={'Pacurar-Laura.pdf'} download={'Pacurar-Laura.pdf'} > Download Resume </a>*/}
                          </div>


                    </Grid>
                </Grid>


                <Footer />
            </div>
        );
    }
}

/*
const RedTitle = styled.h4`
    color: ${props => props.theme.color.red};
`;
*/
const styles = {
    paperContainer: {
        //backgroundImage: `url(${'Laptop.jpg'})`,
        backgroundColor: '#fffdf6',
        position: "fixed",
        backgroundRepeat: "repeat-y",
        height: "auto",
        backgroundSize: "cover",
        backgroundPosition: "center",
        width: "auto",
        minWidth:"100%",
        minHeight:"100%",
        zIndex: "-100"
    },
    textOver: {
        backgroundColor: "#494949",
        color: "white",
        opacity: "0.70",
        //width: "100%",
        //height: "auto",
        textAlign: "center",
        // position: "relative",
        //  paddingTop: "5px",
        // paddingBottom: "10px",
        padding:"20px",
        fontSize: "300%",
        // marginTop: "50px"
    }
};


export default Resume;