import React from 'react';
import styled from 'styled-components';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import {Link} from 'react-router-dom';
import Image from 'material-ui-image';
import Button from '@material-ui/core/Button';
import {withStyles} from '@material-ui/core/styles';
import theme from 'theme';
import Paper from '@material-ui/core/Paper';
import Grid from "@material-ui/core/Grid/Grid";
import MenuBar from '../../components/MenuBar';
import Footer from "components/Footer";
import { SocialIcon } from 'react-social-icons';

//import Avatar from '../../components/Avatar';
import TextField from '@material-ui/core/TextField';


class HomePage extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            videoURL: 'videoHome.mp4'
        };
    }

    render() {
        return (
            <div>
                <MenuBar/>

                <Grid container spacing={24}>
                    <Grid item xs={12}>
                        <video id="background-video" muted  loop autoPlay  style={styles.videoContainer}>
                            <source src={this.state.videoURL} type="video/mp4"/>
                            <source src={this.state.videoURL} type="video/ogg"/>
                            Your browser does not support the video tag.
                        </video>
                    </Grid>

                    <Grid item xs={12} style={{marginTop: "50px"}}>
                        <div style={styles.videoText}>
                            LET'S BUILD SOMETHING AMAZING TOGETHER
                        </div>
                    </Grid>

                    <Grid item xs={12} style={{marginTop: "50px", marginBottom:"500px"}}>
                        <div style={styles.videoTextDescription}>
                            Hi! I'm Laura Pacurar, <br/>
                            a design-minded front-end software engineer <br/> focused on building beautiful interfaces &
                            experiences
                        </div>
                    </Grid>

                   {/* <Grid item xs={12}>
                        <div style={{
                            fontSize: "150%",
                            backgroundColor: "#ece8d9",
                            opacity: "0.70",
                            marginTop: "30px",
                            paddingLeft: "3%",
                            paddingRight: "3%"
                        }}>
                            &nbsp;&nbsp;&nbsp; &nbsp;Squarespace development (my focus)
                            Let's be honest and cut through the marketing fluff. You need a website that looks amazing
                            and
                            actually works. Bottom line, that's what I do. And if you want to learn more about working
                            with
                            me, pick up the phone and give me a call (or email).
                            Squarespace development (my focus)
                            Let's be honest and cut through the marketing fluff. You need a website that looks amazing
                            and
                            actually works. Bottom line, that's what I do. And if you want to learn more about working
                            with
                            me, pick up the phone and give me a call (or email).
                            Squarespace development (my focus) <br/><br/>
                            &nbsp;&nbsp;&nbsp;&nbsp;Let's be honest and cut through the marketing fluff. You need a
                            website that looks amazing
                            and
                            actually works. Bottom line, that's what I do. And if you want to learn more about working
                            with
                            me, pick up the phone and give me a call (or email).
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
                            ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
                            laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in
                            voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat
                            cupidatat
                            non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.

                        </div>
                    </Grid>*/}
                </Grid>
                {/*<Avatar />*/}

                <Footer />
            </div>

        );
    }
}


const styles = {
    videoContainer: {
        backgroundPosition: "center",
        position: 'fixed',
        minWidth: '100%',
        minHeight: '100%',
        width: 'auto',
        height: 'auto',
        zIndex: '-100',
    },
    videoText: {
        backgroundColor: "#494949",
        color: "white",
        opacity: "0.60",
        // width: "100%",
        // height: "auto",
        textAlign: "center",
        // position: "absolute",
         paddingTop: "10px",
         paddingBottom: "10px",
        fontSize: "300%",
        // marginTop: "90px"

    },
    videoTextDescription:
        {
            backgroundColor: "#494949",
            color: "white",
            opacity: "0.50",
            // width: "100%",
            // height: "auto",
            textAlign: "center",
            // position: "absolute",
            paddingTop: "40px",
            paddingBottom: "40px",
            fontSize: "200%",
            // marginTop: "70px"
        }
};
/*
const RedTitle = styled.h4`
    color: ${props => props.theme.color.red};
`;*/

export default HomePage;