import React from 'react';
import styled from 'styled-components';
import Grid from '@material-ui/core/Grid';
import Paper from "@material-ui/core/Paper/Paper";
import Image from "material-ui-image";
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import MenuBar from "components/MenuBar";
import Footer from "components/Footer";
import Button from '@material-ui/core/Button';
import Modal from 'react-modal';
import HoverImage from "react-hover-image";
import CloseRounded from '@material-ui/icons/CloseRounded';


class Portofolio extends React.Component {




    constructor () {
        super();
        this.state = {
            spacing: '24',
            showModal: false,
            text: '',
            title: '',
            isHovering: false

        };

       // this.handleOpenModal = this.handleOpenModal.bind(this);
        this.handleCloseModal = this.handleCloseModal.bind(this);
       // this.handleText = this.handleText.bind(this, newText, newTitle);
        // this.handleMouseHover = this.handleMouseHover.bind(this);
    }

    /*handleOpenModal (newText, newTitle) {
        this.setState({ showModal: true , text: newText, title: newTitle });
    }*/

    // handleOpenModal () {
    //     this.setState({showModal: true});
    // }

    handleCloseModal () {
        this.setState({ showModal: false, text: '' , title: ''});
    }

    handleText (newText, newTitle) {
        this.setState({ text: newText, title: newTitle, showModal: true} );

    }

    // handleMouseHover() {
    //     this.setState(this.toggleHoverState);
    // }
    //
    // toggleHoverState(state) {
    //     return {
    //         isHovering: !state.isHovering,
    //     };
    // }
    componentWillMount() {
        Modal.setAppElement('body');
    }


    render() {

        const { classes } = this.props;
        const { spacing } = this.state;

        return (
            <div>


                <Modal
                    isOpen={this.state.showModal}
                >
                    <Paper style={stylePortofolio.paperContainer}>
                    </Paper>
                    <Button onClick={this.handleCloseModal} style={{marginLeft:"85%"}}>Close </Button>

                    <div style={{ fontSize:"150%", paddingLeft:"15%",paddingBottom:"10%", paddingTop:"5%",paddingRight:"15%"}}>
                        <h3>{this.state.title}</h3> <br/>
                        {this.state.text}</div>

                    <Button onClick={this.handleCloseModal} style={{marginLeft:"85%", marginTop:"5%"}}>Close </Button>

                </Modal>

                <MenuBar />
                <Grid container spacing={24}>
                    <Grid item xs={12}>
                        <Paper style={stylePortofolio.paperContainer}>
                        </Paper>
                    </Grid>

                    <Grid item xs={12} style={{marginTop: "50px", marginBottom:"60px"}}>
                        <div style={stylePortofolio.textOver}>
                            PORTFOLIO
                        </div>
                    </Grid>

                </Grid>

                    <Grid container className={classes.root} spacing={Number(spacing)}>
                        <Grid item xs={12}>
                            <Grid container  justify="center" spacing={Number(spacing)}>
                                <Grid item>

                                    {/*{*/}
                                    {/*let newText = 'text nou';*/}
                                    {/*let newTitle =' titlu nou';*/}
                                   {/*// this.handleText(newText, newTitle);*/}
                                    {/*}*/}

                                    <HoverImage  src={`${'Poza1.jpeg'}`}
                                         style={{cursor:"pointer", height: 300, width: 300}}
                                         onClick={this.handleText.bind(this, 'TEXT1         Lorem ipsum dolor sit amet, sea no novum fabellas, no alia assentior vim, et usu adipisci reprehendunt. Eu duo utroque splendide contentiones. Ea congue argumentum mei. Quot nibh facer ut sed, per audire imperdiet ne. Duo ea unum putant. Ad quo ludus utroque scriptorem.\n' +
                                             '\n' +
                                             'No dicant consequuntur qui, duo id rebum meliore, id vel omnis atqui nostrum. Pro falli liberavisse ne. Id pro tamquam tincidunt. Soluta platonem temporibus nec ex. Ea stet deserunt vim, verear habemus ne per, ex dicta nostro eloquentiam nec. Te his aeque affert officiis, no vim accusamus deseruisse sententiae.', 'CARLI PROJECT')
                                                 }
                                                 hoverSrc={"Poza1B.jpg"}
                                         // onMouseEnter={this.handleMouseHover}
                                         // onMouseLeave={this.handleMouseHover}

                                    />
                                    {/*{console.log('text', this.state.text)}*/}
                                    {/*{*/}
                                        {/*this.state.isHovering &&*/}
                                        {/*de aratat icon cand hover*/}
                                        {/**/}
                                    {/*}*/}


                               {/*Open modal( with parameters-title and text) for every item(image ) from portfolio*/}

                                </Grid>
                                <Grid item>

                                    <HoverImage  src={`${'Poza2.jpeg'}`}
                                                 style={{cursor:"pointer", height: 300, width: 300}}
                                                 onClick={this.handleText.bind(this, 'Text 2             Lorem ipsum dolor sit amet, sea no novum fabellas, no alia assentior vim, et usu adipisci reprehendunt. Eu duo utroque splendide contentiones. Ea congue argumentum mei. Quot nibh facer ut sed, per audire imperdiet ne. Duo ea unum putant. Ad quo ludus utroque scriptorem.\n' +
                                                     '\n' +
                                                     'No dicant consequuntur qui, duo id rebum meliore, id vel omnis atqui nostrum. Pro falli liberavisse ne. Id pro tamquam tincidunt. Soluta platonem temporibus nec ex. Ea stet deserunt vim, verear habemus ne per, ex dicta nostro eloquentiam nec. Te his aeque affert officiis, no vim accusamus deseruisse sententiae. ', 'SUPERNATURAL  ')}
                                                 hoverSrc={"Poza2B.jpg"}
                                    />
                                </Grid>
                                <Grid item>

                                    <HoverImage  src={`${'Poza3.jpeg'}`}
                                                 style={{cursor:"pointer", height: 300, width: 300}}
                                                 onClick={this.handleText.bind(this, 'Text 3            Lorem ipsum dolor sit amet, sea no novum fabellas, no alia assentior vim, et usu adipisci reprehendunt. Eu duo utroque splendide contentiones. Ea congue argumentum mei. Quot nibh facer ut sed, per audire imperdiet ne. Duo ea unum putant. Ad quo ludus utroque scriptorem.\n' +
                                                     '\n' +
                                                     'No dicant consequuntur qui, duo id rebum meliore, id vel omnis atqui nostrum. Pro falli liberavisse ne. Id pro tamquam tincidunt. Soluta platonem temporibus nec ex. Ea stet deserunt vim, verear habemus ne per, ex dicta nostro eloquentiam nec. Te his aeque affert officiis, no vim accusamus deseruisse sententiae.  ', 'SKYSCRAPPER ')}
                                                 hoverSrc={"Poza3B.jpg"}
                                    />
                                </Grid>
                                <Grid item>

                                    <HoverImage  src={`${'Poza4.jpeg'}`}
                                                 style={{cursor:"pointer", height: 300, width: 300}}
                                                 onClick={this.handleText.bind(this, 'Text 4         Lorem ipsum dolor sit amet, sea no novum fabellas, no alia assentior vim, et usu adipisci reprehendunt. Eu duo utroque splendide contentiones. Ea congue argumentum mei. Quot nibh facer ut sed, per audire imperdiet ne. Duo ea unum putant. Ad quo ludus utroque scriptorem.\n' +
                                                     '\n' +
                                                     'No dicant consequuntur qui, duo id rebum meliore, id vel omnis atqui nostrum. Pro falli liberavisse ne. Id pro tamquam tincidunt. Soluta platonem temporibus nec ex. Ea stet deserunt vim, verear habemus ne per, ex dicta nostro eloquentiam nec. Te his aeque affert officiis, no vim accusamus deseruisse sententiae.', 'TRAVEL CITY')}
                                                 hoverSrc={"Poza4B.jpg"}
                                    />
                                </Grid>


                            </Grid>
                            <Grid container  justify="center" spacing={Number(spacing)}>
                                <Grid item>

                                    <HoverImage  src={`${'Poza5.jpeg'}`}
                                                 style={{cursor:"pointer", height: 300, width: 300}}
                                                 onClick={this.handleText.bind(this, 'Text 5      Lorem ipsum dolor sit amet, sea no novum fabellas, no alia assentior vim, et usu adipisci reprehendunt. Eu duo utroque splendide contentiones. Ea congue argumentum mei. Quot nibh facer ut sed, per audire imperdiet ne. Duo ea unum putant. Ad quo ludus utroque scriptorem.\n' +
                                                     '\n' +
                                                     'No dicant consequuntur qui, duo id rebum meliore, id vel omnis atqui nostrum. Pro falli liberavisse ne. Id pro tamquam tincidunt. Soluta platonem temporibus nec ex. Ea stet deserunt vim, verear habemus ne per, ex dicta nostro eloquentiam nec. Te his aeque affert officiis, no vim accusamus deseruisse sententiae. ', 'DOG SHELTER')}
                                                 hoverSrc={"Poza5B.jpg"}
                                    />
                                </Grid>
                                <Grid item>

                                    <HoverImage  src={`${'Poza6.jpeg'}`}
                                                 style={{cursor:"pointer", height: 300, width: 300}}
                                                 onClick={this.handleText.bind(this, 'Text 6            Lorem ipsum dolor sit amet, sea no novum fabellas, no alia assentior vim, et usu adipisci reprehendunt. Eu duo utroque splendide contentiones. Ea congue argumentum mei. Quot nibh facer ut sed, per audire imperdiet ne. Duo ea unum putant. Ad quo ludus utroque scriptorem.\n' +
                                                     '\n' +
                                                     'No dicant consequuntur qui, duo id rebum meliore, id vel omnis atqui nostrum. Pro falli liberavisse ne. Id pro tamquam tincidunt. Soluta platonem temporibus nec ex. Ea stet deserunt vim, verear habemus ne per, ex dicta nostro eloquentiam nec. Te his aeque affert officiis, no vim accusamus deseruisse sententiae. ', 'VINE PROJECT ')}
                                                 hoverSrc={"Poza6B.jpg"}
                                    />
                                </Grid>
                                <Grid item>

                                    <HoverImage  src={`${'Poza1.jpeg'}`}
                                                 style={{cursor:"pointer", height: 300, width: 300}}
                                                 onClick={this.handleText.bind(this, 'Text 7            Lorem ipsum dolor sit amet, sea no novum fabellas, no alia assentior vim, et usu adipisci reprehendunt. Eu duo utroque splendide contentiones. Ea congue argumentum mei. Quot nibh facer ut sed, per audire imperdiet ne. Duo ea unum putant. Ad quo ludus utroque scriptorem.\n' +
                                                     '\n' +
                                                     'No dicant consequuntur qui, duo id rebum meliore, id vel omnis atqui nostrum. Pro falli liberavisse ne. Id pro tamquam tincidunt. Soluta platonem temporibus nec ex. Ea stet deserunt vim, verear habemus ne per, ex dicta nostro eloquentiam nec. Te his aeque affert officiis, no vim accusamus deseruisse sententiae. ', 'CARLI PROJECT')}
                                                 hoverSrc={"Poza1B.jpg"}
                                    />
                                </Grid>
                                <Grid item>

                                    <HoverImage  src={`${'Poza2.jpeg'}`}
                                                 style={{cursor:"pointer", height: 300, width: 300}}
                                                 onClick={this.handleText.bind(this, 'Text 8             Lorem ipsum dolor sit amet, sea no novum fabellas, no alia assentior vim, et usu adipisci reprehendunt. Eu duo utroque splendide contentiones. Ea congue argumentum mei. Quot nibh facer ut sed, per audire imperdiet ne. Duo ea unum putant. Ad quo ludus utroque scriptorem.\n' +
                                                     '\n' +
                                                     'No dicant consequuntur qui, duo id rebum meliore, id vel omnis atqui nostrum. Pro falli liberavisse ne. Id pro tamquam tincidunt. Soluta platonem temporibus nec ex. Ea stet deserunt vim, verear habemus ne per, ex dicta nostro eloquentiam nec. Te his aeque affert officiis, no vim accusamus deseruisse sententiae.  ', 'SUPERNATURAL')}
                                                 hoverSrc={"Poza2B.jpg"}
                                    />
                                </Grid>


                            </Grid>

                            <Grid container  justify="center" spacing={Number(spacing)}>
                                <Grid item>

                                    <HoverImage  src={`${'Poza3.jpeg'}`}
                                                 style={{cursor:"pointer", height: 300, width: 300}}
                                                 onClick={this.handleOpenModal}
                                                 hoverSrc={"Poza3B.jpg"}
                                    />
                                </Grid>
                                <Grid item>

                                    <HoverImage  src={`${'Poza4.jpeg'}`}
                                                 style={{cursor:"pointer", height: 300, width: 300}}
                                                 onClick={this.handleOpenModal}
                                                 hoverSrc={"Poza4B.jpg"}
                                    />
                                </Grid>
                                <Grid item>

                                    <HoverImage  src={`${'Poza2.jpeg'}`}
                                                 style={{cursor:"pointer", height: 300, width: 300}}
                                                 onClick={this.handleOpenModal}
                                                 hoverSrc={"Poza2B.jpg"}
                                    />
                                </Grid>
                                <Grid item>

                                    <HoverImage  src={`${'Poza5.jpeg'}`}
                                                 style={{cursor:"pointer", height: 300, width: 300}}
                                                 onClick={this.handleOpenModal}
                                                 hoverSrc={"Poza5B.jpg"}
                                    />
                                </Grid>


                            </Grid>


                            <Grid container  justify="center" spacing={Number(spacing)}>
                                {[0, 1, 2, 3 ].map(value => (
                                    <Grid key={value} item>
                                        <Paper className={classes.paper} />
                                    </Grid>
                                ))}

                            </Grid>
                        </Grid>

                    </Grid>

                    <Footer />

            </div>
        );
    }
}

/*const RedTitle = styled.h4`
    color: ${props => props.theme.color.red};
`;*/
const stylePortofolio={
    paperContainer: {
       // backgroundImage: `url(${'Laptop.jpg'})`,
        backgroundColor: '#fffdf6',
        position: "fixed",
        backgroundRepeat: "repeat-y",
        height: "auto",
        backgroundSize: "cover",
        backgroundPosition: "center",
        width: "auto",
        minWidth:"100%",
        minHeight:"100%",
        zIndex: "-100"
    },
    textOver: {
        backgroundColor: "#494949",
        color: "white",
        opacity: "0.70",
        //width: "100%",
        //height: "auto",
        textAlign: "center",
        // position: "relative",
        //  paddingTop: "5px",
        // paddingBottom: "10px",
        padding:"20px",
        fontSize: "300%",
        // marginTop: "50px"
    }

};
const styles = theme => ( {
    root: {
        flexGrow: 1,
        padding: "20px"
    },
    paper: {
        height: 300,
        width: 300,
       // position: "relative"
    },
    control: {
        padding: theme.spacing.unit * 2,
    },

});
export default withStyles(styles)(Portofolio);