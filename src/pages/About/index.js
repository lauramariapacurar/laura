import React from 'react';
import styled from 'styled-components';
import Paper from "@material-ui/core/Paper/Paper";
import Image from 'material-ui-image';
import Grid from "@material-ui/core/Grid/Grid";
import MenuBar from "components/MenuBar";
import TextField from "@material-ui/core/TextField/TextField";
import {SocialIcon} from "react-social-icons";
import Footer from 'components/Footer';

class About extends React.Component {
    render() {
        return (
            <div>

               <MenuBar />
                <Grid container spacing={24}>
                    <Grid item xs={12} sm={12} md={12}>
                        <Paper style={styles.paperContainer}>
                        </Paper>
                    </Grid>

                    <Grid item  xs={12} sm={12} md={12} style={{marginTop: "50px", marginBottom:"60px"}}>
                        <div style={styles.textOver}>
                            ABOUT ME
                        </div>
                    </Grid>
                    {/*<Grid item xs={1} sm={1} md={1} >*/}
                    {/*</Grid>*/}

                    {/*Left Side Text */}


                    <Grid item xs={12} sm={12} md={8}  style={{padding:"16px", marginBottom:"70px"}}>
                        <div style={{fontSize:"200%", backgroundColor:"#fffdf6",opacity:"0.65", paddingLeft: "15%", marginBottom:"2%"}}>
                            <b>PERSONAL DETAILS</b> <br/>
                            Understand who I am as a person
                        </div>
                        <div style={{ display: "block", fontSize:"150%", backgroundColor:"#fffdf6",opacity:"0.65", paddingLeft: "15%",}} >
                             Hi! I'm Laura Pacurar, <br/>
                            a design-minded front-end software engineer  focused on building beautiful interfaces &
                            experiences.
                            Let's be honest and cut through the marketing fluff. You need a website that looks amazing
                            and
                            actually works. Bottom line, that's what I do. And if you want to learn more about working
                            with
                            me, pick up the phone and give me a call (or email). <br/>

                        </div>
                    </Grid>
                    {/*<Grid item xs={12} sm={12} md={1} >*/}
                    {/*</Grid>*/}


                    {/* Portret Image*/}


                    <Grid item xs={12} sm={12} md={3}  >
                        <img src="/Laura.jpeg" style={{height:"270px", opacity:"0.90", paddingLeft:"10%", border:"6px" }}/>
                    </Grid>



                    {/*Bottom Side Text*/}

                    <Grid item xs={12}>
                        <div style={{
                            fontSize: "150%",
                            backgroundColor: "#ece8d9",
                            opacity: "0.70",
                            marginTop: "30px",
                            paddingLeft: "10%",
                            paddingRight: "10%",
                            marginBottom:"5%"
                        }}>
                            &nbsp;&nbsp;&nbsp; &nbsp;Squarespace development (my focus)
                            Let's be honest and cut through the marketing fluff. You need a website that looks amazing
                            and
                            actually works. Bottom line, that's what I do. And if you want to learn more about working
                            with
                            me, pick up the phone and give me a call (or email).
                            Squarespace development (my focus)
                            Let's be honest and cut through the marketing fluff. You need a website that looks amazing
                            and
                            actually works. Bottom line, that's what I do. And if you want to learn more about working
                            with
                            me, pick up the phone and give me a call (or email).
                            Squarespace development (my focus) <br/><br/>
                            &nbsp;&nbsp;&nbsp;&nbsp;Let's be honest and cut through the marketing fluff. You need a
                            website that looks amazing
                            and
                            actually works. Bottom line, that's what I do. And if you want to learn more about working
                            with
                            me, pick up the phone and give me a call (or email).
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
                            ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
                            laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in
                            voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat
                            cupidatat
                            non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.

                        </div>
                    </Grid>


                </Grid>

                <Footer />
            </div>
        );
    }
}

/*
const RedTitle = styled.h4`
    color: ${props => props.theme.color.red};
`;*/
const styles = {
    paperContainer: {
       // backgroundImage: `url(${'Laptop.jpg'})`,
        backgroundColor: '#fffdf6',
        position: "fixed",
        backgroundRepeat: "repeat-y",
        height: "auto",
        backgroundSize: "cover",
        backgroundPosition: "center",
        width: "auto",
        minWidth:"100%",
        minHeight:"100%",
        zIndex: "-100"
    },
    textOver: {
        backgroundColor: "#494949",
        color: "white",
        opacity: "0.70",
        //width: "100%",
        //height: "auto",
        textAlign: "center",
       // position: "relative",
      //  paddingTop: "5px",
       // paddingBottom: "10px",
        padding:"20px",
        fontSize: "300%",
       // marginTop: "50px"
    }
};

export default About;